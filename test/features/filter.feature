Feature: Filtering Functionality

    Background:
        Given I navigate to showroom page

    Scenario Outline: Filter lease cars by "Best Deals" on popular filters
        When I select the 'Popular filters' from filter tab
        When I select the 'Best deals' checkbox
        Then I should see the results for 'Best deals' on the page

    Scenario Outline: Filter lease cars by "No stress plan" on popular filters
        When I select the 'Popular filters' from filter tab
        When I select the 'No stress plan' checkbox
        Then I should see the results for 'No stress plan' on the page

    Scenario Outline: Filter lease cars by "Make & Model"
        When I select the 'Make & Model' from filter tab
        When I select the 'Skoda' checkbox on popular makes
        Then I should see the results for 'Skoda' on the page

    Scenario Outline: Select multiple filters
        When I select the 'Popular filters' from filter tab
        When I select the 'Best deals' checkbox
        When I select the 'Make & Model' from filter tab
        When I select the 'Skoda' checkbox on popular makes
        Then I should see the results for 'Skoda' on the page
        Then I should see the results for 'Best deals' on the page
        
    Scenario Outline: Search with invalid Make name - Negative Case
        When I select the 'Make & Model' from filter tab
        When I search for a random make
        Then I should not see any results



