const {
    Given,
    When,
    Then
} = require('@cucumber/cucumber');
const Showroom = require('../pageobjects/showroom.page')

Given("I navigate to showroom page", () => {
    Showroom.open();
    Showroom.acceptCookies();
});

When("I select the {string} from filter tab", (mainFilter) => {
    Showroom.clickonMainFilter(mainFilter);
});

When("I select the {string} checkbox", (checkbox) => {
    Showroom.clickonCheckBox(checkbox);
});

Then("I should see the results for {string} on the page", (control) => {
    Showroom.checkLabelAdded(control);
});

When("I select the filter {string} as a body type", (bodyType) => {
    Showroom.clickonQuickFilter(bodyType);
});

When("I adjust monthly price between {int} and {int} euros", (min, max) => {
    Showroom.filterBetweenNumbers(min,max);
});

When("I click on one of the vehicle cards randomly", () => {
    Showroom.selectVehicleCardRandom();
});

When("I should see that {string} is body type of vehicle", (bodyType) => {
    Showroom.checkBodyType(bodyType);
}); 

When("I select the {string} checkbox on popular makes", (make) => {
    Showroom.clickonCheckBoxMake(make);
}); 

When("I search for a random make", () => {
    Showroom.searchWithRandomString();
}); 

Then("I should not see any results", () => {
    Showroom.checkTheResultsofSearch();
}); 



