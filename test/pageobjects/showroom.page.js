const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class Showroom extends Page {
    /**
     * define selectors using getter methods
     */
    get acceptCookiesButton() { return $('[title="Accept Cookies"]') }
    mainFilterType(item) { return $$('h3=' + item + '')[1] }
    checkBoxType(item) { return $('//*[@id="' + item + '"]/parent::label/span') }
    bodyTypeName(item) { return $('[data-key="' + item + '"]') }
    get vehicleCard() { return $$('[data-component="VehicleCard"]') }
    get bodyTypeLabel() { return $('//*[@data-key="bodyType"]') }
    get slider() { return $$('//*[@role="slider"]') }
    get header() { return $('<h1 />') }
    get deleteButton() { return $('[data-key="resetFilters"]') }
    get searchMake() { return $('[data-e2e-text-input-input]') }
    get makeList() { return $$('//*[@name="make"]') }

    /**
     * define methods
     */

    acceptCookies() {
        if (this.acceptCookiesButton.isDisplayed()) {
            this.acceptCookiesButton.click();
        }
    }

    clickonMainFilter(mainFilter) {
        this.mainFilterType(mainFilter).waitForClickable();
        this.mainFilterType(mainFilter).click();
    }

    clickonCheckBox(checkbox) {
        this.checkBoxType(checkbox).waitForClickable();
        this.checkBoxType(checkbox).click();
    }
    
    clickonQuickFilter(bodyType) {
        this.bodyTypeName(bodyType).waitForClickable();
        this.bodyTypeName(bodyType).click();
    }

    checkLabelAdded(label) {
        for (let i = 0; i < this.vehicleCard.length; i++) {
            this.vehicleCard[i].waitForDisplayed();
            expect(this.vehicleCard[i].getText()).toContain(label);
        }
    }

    selectVehicleCardRandom() {
        var randomInt = Math.floor(Math.random() * (this.vehicleCard.length + 1));
        this.vehicleCard[randomInt].waitForClickable();
        this.vehicleCard[randomInt].click();
    }

    checkBodyType(bodyTypeVehicle) {
        this.bodyTypeLabel.waitForDisplayed();
        expect(this.bodyTypeLabel.getText()).toEqual(bodyTypeVehicle);
    }

    clickonCheckBoxMake(make) {
        var makeLabel = "make-" + make.toUpperCase();
        this.checkBoxType(makeLabel).waitForClickable();
        this.checkBoxType(makeLabel).click();
    }

    checkHeaderIncludes(brand) {
        expect(this.header.getText()).toContain(brand);
    }

    filterBetweenNumbers(min, max) {
        this.slider[0].dragAndDrop({ x: min, y: 0 });
        this.slider[1].dragAndDrop({ x: max, y: 0 })
    }

    clickonDeleteButton() {
        this.deleteButton.click();
    }

    searchWithRandomString() {
        var randomString = Math.random().toString(36).substr(2, 5);
        this.searchMake.setValue(randomString);
    }

    checkTheResultsofSearch() {
        expect(this.makeList.length).toEqual(0);
    }
}

module.exports = new Showroom();
