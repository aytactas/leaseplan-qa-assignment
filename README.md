# LeasePlan-QA-Assignment

## Introduction

### Technologies
- Webdriver.io
- Javascript
- Cucumber
- Gitlab
- Allure Report

## Prerequisites

1. install Node >= 12.9.1
2. install npm >= 6.10.3

## Setup

### Running from local
1. Clone the repository into your local
    https://gitlab.com/aytactas/leaseplan-qa-assignment.git
2. Run the following commands on the same directory
```
    npm install
    npm run test
    npm run report
```
3. Check the generated html report on allure-report directory

### Running on Gitlab CI
1. Navigate to CI/CD -> Pipelines on left section
2. Click on a "Run Pipeline" button
3. Browse the HTML report as artifact

## Tests
- Filter lease cars by "Best Deals" on popular filters
- Filter lease cars by "No stress plan" on popular filters
- Filter lease cars by "Make & Model"
- Select multiple filters
- Search with invalid Make name - Negative Case

## How to Write New Test Cases
1. Scenario should be added to the .feature file in Gherkin format (Given When Then)
2. Step definitions should be defined at step_definitions/steps.js file
3. Page object model should be followed and each method and selector should be added to the related page object page
4. 